(function(){
  'use strict';

  window.vox = {

    isScrolledIntoView : function (elem) {
      // Visible on the screen
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();
      var elemTop = $(elem).offset().top;
      return (elemTop <= docViewBottom);
    },

  	clickHandler : {

      navMenu : function (){
        $('.navbar-toggle').click(function() {
          $('body').toggleClass('menu-open');

        }); 

      },

      formNextBtn : function() {
        $('#form-section .btn').click(function() {
          $('#form-section .active').addClass('visited');
        });
      },

      init: function() {
        this.navMenu();
        this.formNextBtn();
      }
    },

    carouselHandler : {

      stopAutoScroll : function() {
        $('.carousel').carousel({
          interval: false
        });
      },

      scrollOnHover : function() {
        $('#theCarousel').carousel({
          interval: false
        });

        var i;

        $('.carousel-control').on("mouseover", function () {
            var control = $(this),
                interval = 500;

            i = setInterval(function () {
                control.trigger("click");
            }, interval);
        })
        
        .on("mouseout", function () {
            clearInterval(i);
        });
      },

      scrollByOne : function() {
        $('#theCarousel .item').each(function(){
          var itemToClone = $(this);

          for (var i=1;i<4;i++) {
            itemToClone = itemToClone.next();

            // wrap around if at end of item collection
            if (!itemToClone.length) {
              itemToClone = $(this).siblings(':first');
            }

            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
              .addClass("cloneditem-"+(i))
              .appendTo($(this));
          }
        });
      },

      init : function() {
        this.stopAutoScroll();
        //this.scrollOnHover();
        this.scrollByOne();
      }
    },

    runAnimation : {

      heroAnimation: function() {
        $('body').addClass('welcome');
      }
    },

    scrollTo : {

      tweetScroll: function() {
        if(vox.isScrolledIntoView('.tweet')) {
          $('body').addClass('show-tweet');
        }

        $(window).scroll(function(){
          if(vox.isScrolledIntoView('.tweet')) {
            $('body').addClass('show-tweet');
          }
        });
      },

      phoneAnimate: function() {
        AOS.init({
          duration: 1200,
        });
      },

      iconAnimate: function() {

        if(vox.isScrolledIntoView('#summary-section img')) {
          $('#summary-section .thumbnail').addClass('in-view');
        }

        $(window).scroll(function(){
          if(vox.isScrolledIntoView('#summary-section img')) {
            $('#summary-section .thumbnail').addClass('in-view');
          }
        });
      },

      videoAnimate: function() {

        if(vox.isScrolledIntoView('.video-content')) {
          $('.video-content').addClass('in-view');
        }

        $(window).scroll(function(){
          if(vox.isScrolledIntoView('.video-content')) {
            $('.video-content').addClass('in-view');
          }
        });
      },

      init: function() {
        this.tweetScroll();
        this.phoneAnimate();
        this.iconAnimate();
        this.videoAnimate();
      }
    },

    init: function(/*settings*/){
      this.clickHandler.init();
      this.carouselHandler.init();
      this.runAnimation.heroAnimation();
      this.scrollTo.init();
    }
  };

})(this);

window.vox.init();
